//
//  ps32shocktimerslot.h
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/23/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//

#ifndef __ps3dualshock__ps32shocktimerslot__
#define __ps3dualshock__ps32shocktimerslot__

#include <stdio.h>

struct timer_slot{
    void (*fhndlr)(void*);
    bool active;
    bool loop;
    void * param;
    struct itimerval timer_configuration;
};
typedef struct timer_slot TIMERSLOT;
typedef TIMERSLOT* pTIMERSLOT;

pTIMERSLOT TIMERSLOTCreate(int,void(*)(void*),void*);   //Milliseconds,fhndlr,param
void TIMERSLOTActivate(pTIMERSLOT,bool);                //Timer Slot, activate
void TIMERSLOTLoops(pTIMERSLOT,bool);                   //Timer Slot, loops
void TIMERSLOTGetPeriodConfiguration(pTIMERSLOT,struct itimerval*);  //Timer Slot, timer_configuration
void TIMERSLOTKill(pTIMERSLOT);



#endif /* defined(__ps3dualshock__ps32shocktimerslot__) */
