//
//  nanoqueue.c
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/20/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//
#include <assert.h>
#include <stdlib.h>
#include <string.h>


#include "nanoqueue.h"
static nanoqueue_error nnq_error;

pQITEM  ITEMCreate(void * pdata, size_t size){
    
    pQITEM pitem = (pQITEM) malloc(sizeof(QITEM));
    pitem -> pdata  = pdata;
    pitem -> size   = size;
    return pitem;
    
}

pQITEM ITEMCopy_(pQITEM pitem, pQITEM(*copyfunc)(pQITEM)){
    
    int index;
    void * chunk;
    
    if (copyfunc) return copyfunc(pitem);
    
    chunk = malloc(sizeof(pitem->size));
    assert(chunk);
    
    for (index=0; index<pitem->size; index++) {
        ((unsigned char*)chunk)[index] = ((unsigned char*)pitem)[index];
    }
    return ITEMCreate(chunk, pitem->size);
}

inline void* ITEMGet(pQITEM pitem){
    return pitem->pdata;
}

size_t ITEMSize(pQITEM pitem){
    return pitem->size;
}

void ITEMDestroy(pQITEM pitem, void(*killhandler)(void*)){    
    if (killhandler) {
        killhandler(pitem->pdata);
    }
    free(pitem);
}



typedef struct nanoqueue {
    
    QUEUE * nano_q;
    int nano_sock;
    
}NANOQUEUE;
typedef NANOQUEUE* pNANOQUEUE;

pNANOQUEUE  NN_QUEUECreate(const char * url, int family);   //Create A PipeLine
void        NN_QUEUEPush(void*,void*,size_t);




pQUEUE  QUEUECreate(unsigned int maxsize){
    
    pQUEUE pq;
    pq = (pQUEUE) malloc(sizeof(*pq));
    assert(pq);
    memset(pq,0,sizeof(*pq));
    pq -> maxsize = maxsize;
    nnq_error=0;
    return pq;
    
}

void QUEUEPut(void * pq, pQITEM pitem, int block, int timeout){
    
    pQUEUE pnnq = (pQUEUE)pq;
    
    //Get new chunk
    pnnq->items = (pQITEM*)realloc(pnnq->items, sizeof(QITEM)*(pnnq->indatacounter+1));
    assert(pnnq->items);
    
    pnnq->items[pnnq->indatacounter]=pitem;
    pnnq->indatacounter++;
    
    
}

void QUEUEFx(void * pq, pQITEM pitem){
    pQUEUE pnnq = (pQUEUE) pq;
    pQUEUE pauxqueue;
    int index=0;
    assert(pnnq->queue_list_to_push);
    pnnq -> outdatacounter++;
    
    
}

















