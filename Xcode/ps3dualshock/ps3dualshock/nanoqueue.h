//
//  nanoqueue.h
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/20/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//

#ifndef __ps3dualshock__nanoqueue__
#define __ps3dualshock__nanoqueue__

#include <stdio.h>

typedef struct QITEM {
    void * pdata;
    size_t size;
}QITEM;
typedef QITEM * pQITEM;

//Object Basic
pQITEM  ITEMCreate(void *, size_t);             //data pointer, data size
pQITEM  ITEMCopy(pQITEM);                       //item
pQITEM  ITEMCopy_(pQITEM,pQITEM(*)(pQITEM));    //item, function to copy item
void*   ITEMGet(pQITEM);
size_t  ITEMSize(pQITEM);
void    ITEMDestroy(pQITEM,void(*)(void*));     //item, handler to kill object



typedef enum nanoqueue_error {
    NANOQUEUE_OK,
    NANOQUEUE_ERROR_CREATE,
    NANOQUEUE_FULL_QUEUE,
    NANOQUEUE_EMPTY_QUEUE
    
} nanoqueue_error;

typedef struct queue{
    
    struct queue * queue_list_to_feed;
    int queue_list_size;
    
    unsigned int indatacounter;
    unsigned int outdatacounter;
    pQITEM * items;
    size_t * pdatasize;
    unsigned int maxsize;
    unsigned int sync_lock_level;
    
    
}QUEUE;
typedef QUEUE * pQUEUE;

pQUEUE  QUEUECreate(unsigned int);                       //maxsize
void    QUEUEPut(void*,pQITEM,int,int);         //q,item,item_size,block,timeout
void    QUEUEFx(void*,pQITEM);                  //q,item,item_size
void    QUEUEConnect(void*,void*);              //q, q to connect
pQITEM  QUEUEGet(void*,int,int);                //q, block, timeout
void    QUEUEFlush(void*);                      //q


#endif /* defined(__ps3dualshock__nanoqueue__) */
