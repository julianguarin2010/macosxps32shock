//
//  ps3sync.h
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/23/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//

#ifndef __ps3dualshock__ps3sync__
#define __ps3dualshock__ps3sync__

#include "ps32shocktimerslot.h"

#define MAX_TIMER_SLOTS 16

#define _1_MILLISEC_TIMER_INDEX     0
#define _10_MILLISEC_TIMER_INDEX    1
#define _100_MILLISEC_TIMER_INDEX   2
typedef struct ps3timersync{
    
    pTIMERSLOT * timer_array[3] ;
    unsigned int allocated_timers[3];
    struct itimerval timer_configuration;
    
    
    
}PS32SHOCKTIMERSYNC;
typedef PS32SHOCKTIMERSYNC*pPS32SHOCKTIMERSYNC;


#endif /* defined(__ps3dualshock__ps3sync__) */
