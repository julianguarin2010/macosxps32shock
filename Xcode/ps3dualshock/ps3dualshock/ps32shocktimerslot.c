//
//  ps32shocktimerslot.c
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/23/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>

#include "ps32shocktimerslot.h"



pTIMERSLOT TIMERSLOTCreate(int millisec_period, void(*fhndlr)(void*),void*param)
{
    pTIMERSLOT pslot;
    
    pslot = (pTIMERSLOT)malloc(sizeof(struct timer_slot));
    assert(pslot);
    
    memset(pslot, 0, sizeof(struct timer_slot));
    
    pslot->fhndlr = fhndlr;
    
    pslot->timer_configuration.it_interval.tv_sec   = millisec_period/1000;
    pslot->timer_configuration.it_interval.tv_usec  = millisec_period%1000;//Usar esta variable como milisegundos.
    pslot->active = false;
    pslot->loop = false;
    pslot->param=param;
    
    return pslot;
}
void TIMERSLOTActivate(pTIMERSLOT pslot, bool activate){
    pslot->active=activate;
}
void TIMERSLOTLoops(pTIMERSLOT pslot,bool loops){
    pslot->loop = loops;
}
void TIMERSLOTGetPeriodConfiguration(pTIMERSLOT pslot, struct itimerval * timer_configuration){
    memcpy(timer_configuration, &(pslot->timer_configuration), sizeof(struct itimerval));
}
void TIMERSLOTKill(pTIMERSLOT pslot){
    free(pslot);
}

