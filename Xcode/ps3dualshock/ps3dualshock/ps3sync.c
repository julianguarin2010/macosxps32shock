//
//  ps3sync.c
//  ps3dualshock
//
//  Created by Julian A Guarin on 3/23/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/time.h>
#include "ps3sync.h"
#include "ps32shocktimerslot.h"




static pPS32SHOCKTIMERSYNC pps3timerunique=0x00;
static bool pps3timerunique_locked=false;


void PS32SHOCKTIMERSYNCInit();
int PS32SHOCKTIMERSYNCAddSlot(pTIMERSLOT);
pTIMERSLOT PS32SHOCKTIMERSYNCGetSlot(int index);
void PS32SHOCKTIMERSYNCRun(int index);
void PS32SHOCKTIMERSYNCPause(int index);
void PS32SHOCKTIMERSYNCReset(int index);


void timer_event(int signal){
    
    int resolution_index,timer_index;
    pTIMERSLOT pslot;
    
    for (resolution_index=0; resolution_index<3; resolution_index++) {
        for (timer_index=0; timer_index<pps3timerunique->allocated_timers[resolution_index]; timer_index++) {
            pslot = pps3timerunique->timer_array[resolution_index][timer_index];
            if (!resolution_index || ) {
                <#statements#>
            }
        }
    }
    
}

void PS32SHOCKTIMERSYNCInit(void){
    if (pps3timerunique) return;
    while (pps3timerunique_locked);
    pps3timerunique_locked=true;
    pps3timerunique = (pPS32SHOCKTIMERSYNC)malloc(sizeof(PS32SHOCKTIMERSYNC));
    assert(pps3timerunique);
    memset(pps3timerunique, 0, sizeof(PS32SHOCKTIMERSYNC));
}
int PS32SHOCKTIMERSYNCAddSlot(pTIMERSLOT pslot){
    
    int index;
    int _10th[] = {1,10,100};
    int ntimers;
    
    struct itimerval timer_configuration;
    if (pps3timerunique->allocated_timers[0] + pps3timerunique->allocated_timers[1] +pps3timerunique->allocated_timers[2] >=MAX_TIMER_SLOTS) {
        return -1;
    }
    
    
    TIMERSLOTGetPeriodConfiguration(pslot, &timer_configuration);
    timer_configuration.it_interval.tv_sec *= 1000;
    timer_configuration.it_interval.tv_sec += timer_configuration.it_interval.tv_usec;
    
    for (index=3; index>=0; index--) {
        if (timer_configuration.it_interval.tv_usec % _10th[index] == 0){
            ntimers = pps3timerunique->allocated_timers[index];
            pps3timerunique->timer_array[index] = (pTIMERSLOT*) realloc(pps3timerunique->timer_array[index], (ntimers+1)*sizeof(pTIMERSLOT));
            assert(pps3timerunique->timer_array[index]);
            pps3timerunique->timer_array[index][ntimers] = pslot;
            pps3timerunique->allocated_timers[index]++;
            return index * MAX_TIMER_SLOTS + pps3timerunique -> allocated_timers[index] - 1;
        }
    }
    return -1;
}
pTIMERSLOT PS32SHOCKTIMERSyncGetSlot(int index){
    
    pTIMERSLOT * line = pps3timerunique->timer_array[ index/MAX_TIMER_SLOTS ];
    return line[index%MAX_TIMER_SLOTS];
    
}
void PS32SHOCKTIMERSYNCRun(int index){
    
}


pNANOTMR    TMRCreate(unsigned int, void(*tmr_hndlr)(void));    //millisecs, expiration handler
void        TMRLoop(pNANOTMR,bool);                              //timer, loops?
void        TMRRun(pNANOTMR);                                   //timer
bool        TMRExpired(pNANOTMR);                               //timer
void        TMRDestroy(pNANOTMR);

void TMRDestroy(pNANOTMR tmr){
    
    free(tmr);
    
}
static void nanotimer_default_handler(void);

pNANOTMR    TMRCreate(unsigned int milliseconds, void(*tmr_hndlr)(void)){

    pNANOTMR tmr;
    unsigned int secs;
    unsigned int usecs;
    
    if (!milliseconds) return 0x0;
    tmr = (pNANOTMR)malloc(sizeof(NANOTMR));
    assert(tmr);
    tmr -> tmr_hndlr = tmr_hndlr;
    
    
    secs    = milliseconds>1000 ? milliseconds/1000 : 0;
    usecs   = (milliseconds%1000) * 1000;
    
    tmr -> timer_config.it_interval.tv_sec  = secs;
    tmr -> timer_config.it_value.tv_sec     = secs;
    tmr -> timer_config.it_interval.tv_usec = usecs;
    tmr -> timer_config.it_value.tv_usec    = usecs;
    
    
    
    return tmr;
    
}

void nanotimer_

static void timer_func();

// ----------------------------------------------------------------------------------------
// begin provided code part
// variables needed for timer
static bool timer_stopped;	// non-zero if the timer is to be timer_stopped

/* Timer signal handler.
 * On each timer signal, the signal handler will signal a semaphore.
 */
static void
timersignalhandler()
{
    /* called in signal handler context, we can only call
     * async-signal-safe functions now!
     */
    sem_post(&timer_sem);	// the only async-signal-safe function pthreads defines
}

/* Timer thread.
 * This dedicated thread waits for posts on the timer semaphore.
 * For each post, timer_func() is called once.
 *
 * This ensures that the timer_func() is not called in a signal context.
 */
static void *
timerthread(void *_)
{
    while (!timer_stopped) {
        int rc = sem_wait(&timer_sem);		// retry on EINTR
        if (rc == -1 && errno == EINTR)
            continue;
        if (rc == -1) {
            perror("sem_wait");
            exit(-1);
        }
        
        timer_func();	// user-specific timerfunc, can do anything it wants
    }
    return 0;
}

/* Initialize timer */
void
init_timer(void)
{
    /* One time set up */
    sem_init(&timer_sem, /*not shared*/ 0, /*initial value*/0);
    pthread_create(&timer_thread, (pthread_attr_t*)0, timerthread, (void*)0);
    signal(SIGALRM, timersignalhandler);
}

/* Shut timer down */
void
shutdown_timer()
{
    timer_stopped = true;
    sem_post(&timer_sem);
    pthread_join(timer_thread, 0);
}

/* Set a periodic timer.  You may need to modify this function. */
void
set_periodic_timer(long delay)
{
    struct itimerval tval = {
        /* subsequent firings */ .it_interval = { .tv_sec = 0, .tv_usec = delay },
        /* first firing */       .it_value = { .tv_sec = 0, .tv_usec = delay }};
    
    setitimer(ITIMER_REAL, &tval, (struct itimerval*)0);
}

// end provided code part
// ----------------------------------------------------------------------------------------

// application-specific part
//
// The following is a simple demonstration of how to use these timers.
//

static sem_t demo_over;	// a semaphore to stop the demo when done

/* Put your timer code in here */
static void
timer_func()
{
    /* The code below is just a demonstration. */
    static int i = 0;
    
    printf ("Timer called %d!\n", i);
    if (++i == 5) {
        shutdown_timer();
        sem_post(&demo_over);	// signal main thread to exit
    }
}

int
main()
{
    sem_init(&demo_over, /*not shared*/ 0, /*initial value*/0);
    
    init_timer();
    set_periodic_timer(/* 200ms */200000);
    
    /* Wait for timer_func to be called five times - note that
     * since we're using signals, sem_wait may fail with EINTR.
     */
    while (sem_wait(&demo_over) == -1 && errno == EINTR)
        continue;
    return 0;
}
